function getData(){
  var data = [];
  data[0] = document.getElementById('rmu').value;
  data[1] = document.getElementById('rk').value;
  data[2] = document.getElementById('rtheta').value;
  data[3] = document.getElementById('rtime').value;
  data[4] = document.getElementById('rreals').value;
  data[5] = document.getElementById('rn').value;
  data[6] = document.getElementById('rm').value;
  console.log(data); // Check the console for the values for every click.
  return data;
}

function count(arr) {
  return arr.reduce((a,b) => (a[b] = a[b] + 1 || 1) && a, {})
}

function intime(mu, k, theta,  N, time, m){
  var x;
  var x2;
  var xb;
  var x2b;
  var N;
  x = 0;
  x2 = 0;
  xb = 0;
  x2b = 0;
  for (i = 0; i <time; i++){
    for (j = 1; j < N + 1; j++){
      ra = Math.random()
      if(Math.random() <= mu){
          x = x + 1
          N = N - 1
        }
      }
    for (j = 1; j < xb + 1; j++){
      if(Math.random() <= theta/(1+((x-1)/k)**m)){
        x = x - 1
        N = N + 1
      }
    }

    xb = x
  }
  return [x]
}

function monte_carlo(){
  var xd1 = getData();
  mu = parseFloat(xd1[0]);
  k = parseFloat(xd1[1]);
  theta = parseFloat(xd1[2]);
  time = parseInt(xd1[3], 10);
  reals = parseInt(xd1[4], 10);
  N = parseInt(xd1[5], 10);
  m = parseFloat(xd1[6]);
  N1 = N ;
  var y1 = [];
  var y2 = [];

  for (i1=0; i1<reals; i1++){
    [ax1] = intime(mu, k, theta, N, time, m);
    y1[i1] = ax1;
    y2[i1] = N1 - y1[i1];
  }
  var Nx0 = []
  var Nx1 = []
  var Nx2 = []
  for (ii=0; ii<= N1; ii++){
    Nx0[ii] = ii;
    Nx1[ii] = count(y1)[ii];
    Nx2[ii] = count(y2)[ii];
    if (typeof Nx1[ii] !== "number") {
      Nx1[ii]= 0;
    }
    if (typeof Nx2[ii] !== "number") {
      Nx2[ii]= 0;
    }
  }
  return [y1, y2, Nx0, Nx1, Nx2]
}

function standardDeviation(values){
  var avg = average(values);
  var squareDiffs = values.map(function(value){
    var diff = value - avg;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });
  var avgSquareDiff = average(squareDiffs);
  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);
  var avg = sum / data.length;
  return avg;
}

function plot(){
  Plotly.purge('myDiv');
  [y1, y2, Nx0, Nx1, Nx2]= monte_carlo()
  var shelter1 = {
    name: "Sheltered",
    x: Nx0,
    y: Nx1,
    type: "bar", 
    marker: { color: '#555555' }
  };
  var outside = {
    name: "Outside",
    x: Nx0,
    y: Nx2,
    type: "bar",
    marker: { color: '#9e9f9e' },
  };
  var data =[shelter1, outside];
  var layout = {
    barmode:"group",
    paper_bgcolor: '#f5f5f5',
    plot_bgcolor: '#f5f5f5',
    xaxis: {
    title: 'Number of individuals',
    titlefont: {
      family: 'Arial',
      size: 18,
      color: '#2e383a'
    }
  },
  xaxis:{
    title: 'Number of individuals',
    ticks: 'outside',
    gridcolor: '#e5e5e5',
    gridwidth: 2,
  },
  yaxis: {
    title: 'Number of realisations',
    ticks: 'outside',
    ticklen: 8,
    titlefont: {
      family: 'Arial',
      size: 18,
      color: '#555555'
    }
  }

  };
  var config = {responsive: true};
  Plotly.newPlot('myDiv', data, layout, config);
}

function plotstats(){
  Plotly.purge('myDiv');
  [y1, y2, Nx0, Nx1, Nx2]= monte_carlo()
  var data = [{
    name: "",
    x: ['Sheltered', 'Outside'],
    y: [average(y1), average(y2)],
    name: 'Control',
    marker: { color: '#555555' },

    error_y: {
      type: 'data',
      array: [standardDeviation(y1), standardDeviation(y2)],
      visible: true
    },
    type: 'bar'
  }];
  var layout = {
    paper_bgcolor: '#f5f5f5',
    plot_bgcolor: '#f5f5f5',
        xaxis: {
    title: '',
    titlefont: {
      family: 'Arial',
      size: 18,
      color: '#2e383a'
    }
  },
  xaxis:{
    title: 'Number of realisations',
    ticks: 'outside',
    gridcolor: '#e5e5e5',
    gridwidth: 2,
  },
  yaxis: {
    ticks: 'outside',
    ticklen: 8,
    title: 'Mean +/- std',
    titlefont: {
      family: 'Arial',
      size: 18,
      color: '#2e383a'
    }
  }
  };
  var config = {responsive: true};
  Plotly.newPlot('myDiv', data, layout, config);
}


function getsampleDataForCSV() {
  [y1, y2, Nx0, Nx1, Nx2]= monte_carlo();
  var rows = [];
  for (i = 0; i < reals; i++) {
    var thisData = [
    i+1,
    y1[i],
    y2[i], 
    y1[i] + y2[i],
    ];
    rows.push(thisData);
  }
  return rows
}

function downloaddata(){
  var sampleData = getsampleDataForCSV();
  return csvGenerator.exportCSV("simulation", sampleData, ["Simulation number", "Shelter 1", "Outside", "Total"]);
}

