// iframe

showIframe = function(url) {
    var divs = document.getElementsByClassName('full');
    for (var i = 0; i < divs.length; i++) {
        divs[i].src = url;
    }
    document.getElementById('frame').style.display = 'block';
}

// map

var map = L.map("map").setView([50.8469377104238, 4.352467785399248], 1);
var layer = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
});
layer.addTo(map);
map.options.minZoom = 8;
map.options.maxZoom = 18;

// Brussels boundaries

var geojsonLayer = new L.GeoJSON.AJAX("Administrative boundaries of Brussels Capital Region.geojson", {
    style: {
        "opacity": 1,
        "weight": .5,
        "fillOpacity": 0.05,
        "color": '#6f44ce'
    }
});
geojsonLayer.addTo(map);

// Places monitored

/// Marais Wiels

var wiels = [50.82574996931262, 4.325808741189292];
var wielsurl = 'https://monitoring.smartwater.brussels/grafana/d/r_popome/marais-wiels?orgId=1&refresh=10s/';
var marker1 = L.marker(wiels).on('click', onClick).addTo(map);
marker1.bindPopup("<h2>Marais Wiels</h2>")

function onClick(e) {
    showIframe(wielsurl);
}

/// Tournay Solvay

var tournaysolvay = [50.794833590172914, 4.40968109222121];
var tournaysolvayurl = 'https://monitoring.smartwater.brussels/grafana/d/3KpWCdj7z/tournesol?orgId=1';
var marker2 = L.marker(tournaysolvay).on('click', onClick1).addTo(map);
marker2.bindPopup("<h2>Tournay Solvay</h2>")

function onClick1(e) {
    showIframe(tournaysolvayurl);
}


/// Musée des Égouts

var museedesegouts = [50.84478890084035, 4.33893407590467];
var museedesegoutsurl = 'https://monitoring.smartwater.brussels/grafana/d/rFhwXtYnk/musee-des-egouts?orgId=1';
var marker3 = L.marker(museedesegouts).on('click', onClick2).addTo(map);
marker3.bindPopup("<h2>Musée des Égouts</h2>")

function onClick2(e) {
    showIframe(museedesegoutsurl);
}

// Bryc

var bryc = [50.88185462114413, 4.374413826166311];
var brycurl = 'https://monitoring.smartwater.brussels/grafana/d/pcTaJnlnk/bryc?orgId=1&refresh=10s';
var marker4 = L.marker(bryc).on('click', onClick3).addTo(map);
marker4.bindPopup("<h2>Bryc</h2>")

function onClick3(e) {
    showIframe(brycurl);
}

// Bound map

// var featureGroup = L.featureGroup([marker1, marker2, marker3]).addTo(map);
// map.fitBounds(featureGroup.getBounds());
geojsonLayer.on('data:loaded', function() {
    this.map.fitBounds(geojsonLayer.getBounds());
}.bind(this));