

Highcharts.setOptions({
    global: {
        useUTC: false
    }
});

var gauge1 = {
    angle: 0,
    lineWidth: 0.2,
    limitMax: 'false', 
    strokeColor: 'red',
    radiusScale: 1,
    generateGradient: true,
      pointer: {
      length: 0.4, // // Relative to gauge radius
      strokeWidth: 0.022, // The thickness
      color: 'black' // Fill color
    },
    staticLabels: {
    font: "80% sans-serif",  // Specifies font
    labels: [0, 4, 5, 8, 10],  // Print labels at these values
    color: "black",  // Optional: Label text color
    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
  },  // just experiment with them
    strokeColor: 'red',   // to see which ones work best for you
    staticZones: [
     {strokeStyle: "#C43933", min: 0, max: 3.99}, // red
     {strokeStyle: "#EF8636", min: 4.01, max: 4.99}, // orange
     {strokeStyle: "#DDDF4B", min: 5.01, max: 7.99}, // Yellow
     {strokeStyle: "#519E3E", min: 8.01, max: 8.49}, 
     // Green
      {strokeStyle: "#3B75AF", min: 8.51, max: 10}, 
     // Blue
  ],
    generateGradient: 'false'
  };
var gauge2 = {
    angle: 0,
    lineWidth: 0.2,
    limitMax: 'false', 
    limitMin: 'false', 

    strokeColor: 'red',
    radiusScale: 1,
    generateGradient: false,
      pointer: {
      length: 0.4, // // Relative to gauge radius
      strokeWidth: 0.022, // The thickness
      color: 'black' // Fill color
    },
    staticLabels: {
    font: "80% sans-serif",  // Specifies font
    labels: [4.5, 5.5, 6, 6.5, 8.5, 9, 9.5, 10.5],  // Print labels at these values
    color: "black",  // Optional: Label text color
    fractionDigits: 1  // Optional: Numerical precision. 0=round off.
  },  // just experiment with them
    strokeColor: 'red',   // to see which ones work best for you
    staticZones: [
    {strokeStyle: "#EF8636", min: 4.5, max: 5.49}, // orange
    {strokeStyle: "#DDDF4B", min: 5.51, max: 5.99}, // Yellow
    {strokeStyle: "#519E3E", min: 6.01, max: 6.49}, // Green
    {strokeStyle: "#3B75AF", min: 6.51, max: 8.49}, // Blue
    {strokeStyle: "#519E3E", min: 8.51, max: 8.99}, // Green
    {strokeStyle: "#DDDF4B", min: 9.01, max: 9.49}, // Yellow
    {strokeStyle: "#EF8636", min: 9.51, max: 10.5}, // orange



  ],
    generateGradient: 'false'
  };

var gauge3 = {
angle: 0,
lineWidth: 0.2,
limitMax: 'false', 
strokeColor: 'red',
radiusScale: 1,
generateGradient: true,
    pointer: {
    length: 0.4, // // Relative to gauge radius
    strokeWidth: 0.022, // The thickness
    color: 'black' // Fill color
},
staticLabels: {
font: "80% sans-serif",  // Specifies font
labels: [0, 20, 23, 25, 28, 35],  // Print labels at these values
color: "black",  // Optional: Label text color
fractionDigits: 0  // Optional: Numerical precision. 0=round off.
},  // just experiment with them
strokeColor: 'red',   // to see which ones work best for you
staticZones: [
    {strokeStyle: "#3B75AF", min: 0, max: 19.99},     // Blue
    {strokeStyle: "#519E3E", min: 20.01, max: 22.99}, // Green
    {strokeStyle: "#DDDF4B", min: 23.01, max: 24.99}, // Yellow
    {strokeStyle: "#EF8636", min: 25.01, max: 27.99}, // orange
    {strokeStyle: "#C43933", min: 28.01, max: 35}, // red
],
generateGradient: 'false'
};

var gauge4 = {
    angle: 0,
    lineWidth: 0.2,
    limitMax: 'false', 
    strokeColor: 'red',
    radiusScale: 1,
    generateGradient: true,
        pointer: {
        length: 0.4, // // Relative to gauge radius
        strokeWidth: 0.022, // The thickness
        color: 'black' // Fill color
    },
    staticLabels: {
    font: "80% sans-serif",  // Specifies font
    labels: [0, 700, 1120, 1350, 1600],  // Print labels at these values
    color: "black",  // Optional: Label text color
    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
    },  // just experiment with them
    strokeColor: 'red',   // to see which ones work best for you
    staticZones: [
        {strokeStyle: "#3B75AF", min: 0, max: 169.9},     // Blue
        {strokeStyle: "#519E3E", min: 170.1, max: 699.9}, // Green
        {strokeStyle: "#DDDF4B", min:700.1, max: 1119.9}, // Yellow
        {strokeStyle: "#EF8636", min: 1120.1, max: 1349.9}, // orange
        {strokeStyle: "#C43933", min: 1350.1, max: 1600}, // red
    ],
    generateGradient: 'false'
    };


var target1 = document.getElementById('container-ox1'); 
var target2 = document.getElementById('container-ph1'); 
var target3 = document.getElementById('container-temp1'); 
var target4 = document.getElementById('container-cond1'); 

// gauge ox
var gaugeox = new Gauge(target1).setOptions(gauge1); 
gaugeox.minValue = 0; 
gaugeox.maxValue = 10;
gaugeox.animationSpeed = 32; 
var target1 = document.getElementById('container-ox1');
gaugeox.set(null);
gaugeox.setTextField(document.getElementById("gauge1-txt"),2);

// gauge ph
var gaugeph = new Gauge(target2).setOptions(gauge2); 
gaugeph.minValue = 4.5; // set max gauge value
gaugeph.maxValue = 10.5; // set max gauge value
gaugeph.animationSpeed = 32; 
var target2 = document.getElementById('container-ph1'); 
gaugeph.set(null);
gaugeph.setTextField(document.getElementById("gauge2-txt"), 2);

// gauge temp
var gaugete = new Gauge(target3).setOptions(gauge3); 
gaugete.minValue = 0; // set max gauge value
gaugete.maxValue = 35; // set max gauge value
gaugete.animationSpeed = 32; 
var target3 = document.getElementById('container-temp1'); // your canvas element
gaugete.set(null);
gaugete.setTextField(document.getElementById("gauge3-txt"), 2);

// gauge cond
var gaugeco = new Gauge(target4).setOptions(gauge4);
gaugeco.minValue = 0; // set max gauge value
gaugeco.maxValue = 1600; // set max gauge value
gaugeco.animationSpeed = 32; 
var target3 = document.getElementById('container-cond1'); // your canvas element
gaugeco.set(null);
gaugeco.setTextField(document.getElementById("gauge4-txt"), 2);



var chartlineox = Highcharts.chart({

    chart: {
        renderTo: 'container-ox2',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        grid: { // default setting
            enabled: true 
          },
        tickWidth: 1,
   
        title: {
            text: ''
        },
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        }
    },
    yAxis: {
        endOnTick: false,
    
        // min: 0,
        // max: 10,
        title: {
            text: ''
        },
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        }
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 0,
            color: '#d62728'
        }, {
            value: 3.9,
            color: '#d62728'
        }, {
            value: 4,
            color: '#ff7f0e'
        }, {
            value: 4.9,
            color: '#ff7f0e'
        }, {
            value: 5,
            color: '#DDDF0D'
        }, {
            value: 7.99,
            color: '#DDDF0D'
        }, {
            value: 8,
            color: '#2ca02c'
        }, {
            value: 8.49,
            color: '#2ca02c'
        }, {
            value: 8.5,
            color: '#1f77b4'
        }]
    }]

});

var chartlineph = Highcharts.chart({
    chart: {
        renderTo: 'container-ph2',
        //   type: 'line',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    time: {
        useUTC: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        },
    },
    yAxis: {
        endOnTick: false,
        // min: 4,
        // max: 11,
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        }
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 4.5,
            color: '#ff7f0e'
        }, {
            value: 5.5,
            color: '#ff7f0e'
        }, {
            value: 5.51,
            color: '#DDDF0D'
        }, {
            value: 6,
            color: '#DDDF0D'
        }, {
            value: 6.01,
            color: '#2ca02c'
        }, {
            value: 6.49,
            color: '#2ca02c'
        }, {
            value: 6.5,
            color: '#1f77b4'
        }, {
            value: 8.49,
            color: '#1f77b4'
        }, {
            value: 8.5,
            color: '#2ca02c'
        }, {
            value: 8.99,
            color: '#2ca02c'
        }, {
            value: 9,
            color: '#DDDF0D'
        }, {
            value: 9.49,
            color: '#DDDF0D'
        }, {
            value: 9.5,
            color: '#ff7f0e'
        }, {
            value: 11,
            color: '#ff7f0e'
        }, ]
    }]
});


var chartlinetemp = Highcharts.chart({
    chart: {
        renderTo: 'container-temp2',
        type: 'line',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    time: {
        useUTC: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        },
    },
    yAxis: {
        // min: 15, 
        // max: 35,
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        }
    },
    rangeSelector: {
        buttons: [{
            text: '+',
            events: {
                click() {
                    return false
                }
            }
        }, {
            text: '-',
            events: {
                click() {
                    return false
                }
            }

        }]
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 0,
            color: '#1f77b4'
        }, {
            value: 19.9,
            color: '#1f77b4'
        }, {
            value: 20,
            color: '#2ca02c'
        }, {
            value: 22.9,
            color: '#2ca02c'
        }, {
            value: 23,
            color: '#DDDF0D'
        }, {
            value: 24.9,
            color: '#DDDF0D'
        }, {
            value: 25,
            color: '#ff7f0e'
        }, {
            value: 27.9,
            color: '#ff7f0e'
        }, {
            value: 28,
            color: '#d62728'
        }, {
            value: 35,
            color: '#d62728'
        }, ]
    }]
});

var chartlineturb = Highcharts.chart({
    chart: {
        renderTo: 'container-turb2',
        // type: 'line',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    credits: {
        enabled: false
    },
    exporting: {
        enabled: false
    },
    time: {
        useUTC: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        },
    },
    yAxis: {
        // min: 400,
        // max: 1700,
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        }
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 100,
            color: '#1f77b4'
        }, {
            value: 169.99,
            color: '#1f77b4'
        }, {
            value: 170,
            color: '#2ca02c'
        }, {
            value: 699.99,
            color: '#2ca02c'
        }, {
            value: 700,
            color: '#DDDF0D'
        }, {
            value: 1119.99,
            color: '#DDDF0D'
        }, {
            value: 1120,
            color: '#ff7f0e'
        }, {
            value: 1349.99,
            color: '#ff7f0e'
        }, {
            value: 1350,
            color: '#d62728'
        }, {
            value: 1600,
            color: '#d62728'
        }, ]
    }]
});


var i1, i2, i3, i4;
i1 = 0;
i2 = 0;
i3 = 0
i4 = 0;


setInterval(function() {
    var xmin, q1, q2, q3, q4, img1, img2, img3, img4, img5;
    img1 = document.getElementById("myimg1");
    img2 = document.getElementById("myimg2");
    img3 = document.getElementById("myimg3");
    img4 = document.getElementById("myimg4");
    img5 = document.getElementById("myimg5");

    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_dissolved_oxygen%22%20WHERE%20time%20%3E%3D%20now()%20-%2090d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(6h)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(dataox) {
            i1 = i1 + 1;
            if (i1 > -1) {
                chartlineox.series[0].setData(dataox.results[0].series[1].values);
            };
            // x = data.results[0].series[1].values[data.results[0].series[1].values.length-1][0];
            x = new Date().getTime();
            y1 = dataox.results[0].series[1].values[dataox.results[0].series[1].values.length - 1][1];
            chartlineox.series[0].setData(dataox.results[0].series[1].values);

            if (gaugeox) {
                gaugeox.set(y1);

            }
            if (chartlineox) {
                if (chartlineox.series[0].length < 10) {
                    chartlineox.series[0].addPoint([x, y1], true, false);
                } else {
                    chartlineox.series[0].addPoint([x, y1], true, true);
                }
            }
            // console.log(dataox.results[0].series[1].values)
        });
    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_pH%22%20WHERE%20time%20%3E%3D%20now()%20-%2090d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(6h)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(dataph) {
            i2 = i2 + 1;
            if (i2 > -1) {
                chartlineph.series[0].setData(dataph.results[0].series[1].values);
            };
            // x = data.results[0].series[1].values[data.results[0].series[1].values.length-1][0];
            x = new Date().getTime();
            y2 = dataph.results[0].series[1].values[dataph.results[0].series[1].values.length - 1][1];
            chartlineph.series[0].setData(dataph.results[0].series[1].values);

            if (gaugeph) {
                gaugeph.set(y2);

            }
            if (chartlineph) {
                if (chartlineph.series[0].length < 10) {
                    chartlineph.series[0].addPoint([x, y2], true, false);
                } else {
                    chartlineph.series[0].addPoint([x, y2], true, true);
                }
            }
            // console.log(dataph.results[0].series[1].values)
        });
    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_temperature%22%20WHERE%20time%20%3E%3D%20now()%20-%2090d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(6h)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(datat) {
            i3 = i3 + 1;
            if (i3 > -1) {
                chartlinetemp.series[0].setData(datat.results[0].series[1].values);
            };
            // x = data.results[0].series[1].values[data.results[0].series[1].values.length-1][0];
            x = new Date().getTime();
            if (i3 == 1) {}
            y3 = datat.results[0].series[1].values[datat.results[0].series[1].values.length - 1][1];
            chartlinetemp.series[0].setData(datat.results[0].series[1].values);

            if (gaugete) {
                gaugete.set(y3);
            }
            if (chartlinetemp) {
                if (chartlinetemp.series[0].length < 10) {
                    chartlinetemp.series[0].addPoint([x, y3], true, false);
                } else {
                    chartlinetemp.series[0].addPoint([x, y3], true, true);
                }
            }
            // console.log(datat.results[0].series[1].values)
        });
    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_conductivity%22%20WHERE%20time%20%3E%3D%20now()%20-%2090d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(6h)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(datac) {
            i4 = i4 + 1;
            if (i4 > -1) {
                chartlineturb.series[0].setData(datac.results[0].series[1].values);
            };
            // x = data.results[0].series[1].values[data.results[0].series[1].values.length-1][0];
            x = new Date().getTime();
            y4 = datac.results[0].series[1].values[datac.results[0].series[1].values.length - 1][1];
            chartlineturb.series[0].setData(datac.results[0].series[1].values);

            if (gaugeco) {
               gaugeco.set(y4);
            }
            if (chartlineturb) {
                if (chartlineturb.series[0].length < 10) {
                    chartlineturb.series[0].addPoint([x, y4], true, false);
                } else {
                    chartlineturb.series[0].addPoint([x, y4], true, true);
                }
            }
            // console.log(datac.results[0].series[1].values)
        });
    if (y1 >= 8) {
        q1 = 5;
    } else if (y1 < 8 && y1 >= 6) {
        q1 = 4;
    } else if (y1 < 6 && y1 >= 4) {
        q1 = 3;
    } else if (y1 < 4 && y1 >= 3) {
        q1 = 2;
    } else {
        q1 = 1;
    }
    if (y2 >= 6.5 && y2 <= 8.5) {
        q2 = 5;
    } else if (y2 < 6.5 && y2 >= 6 || y2 > 8.5 && y2 <= 9) {
        q2 = 4;
    } else if (y2 < 6 && y2 >= 5.5 || y2 > 9 && y2 <= 9.5) {
        q2 = 3;
    } else {
        q2 = 2;
    }
    if (y3 <= 23) {
        q3 = 5;
    } else if (y3 > 23 && y3 <= 25) {
        q3 = 4;
    } else if (y3 > 25 && y3 <= 27.5) {
        q3 = 3;
    } else if (y3 > 27.5 && y3 <= 30) {
        q3 = 2;
    } else {
        q3 = 1;
    }
    if (y4 <= 675) {
        q4 = 5;
    } else if (y4 > 675 && y4 <= 900) {
        q4 = 4;
    } else if (y4 > 900 && y4 <= 1125) {
        q4 = 3;
    } else if (y4 > 1125 && y4 <= 1350) {
        q4 = 2;
    } else {
        q4 = 1;
    }

    // xmin = Math.min(q1, q2, q3, q4);
    xmin = Math.min(q2, q3, q4);

    console.log(y1, q1, y2, q2, y3, q3, y4, q4, xmin);
    if (xmin == 5) {
        // msg1.style.display = "block";
        // msg2.style.display = 'none';
        // msg3.style.display = 'none';
        // msg4.style.display = 'none';
        // msg5.style.display = 'none';
        img1.style.display = 'block';
        img2.style.display = 'none';
        img3.style.display = 'none';
        img4.style.display = 'none';
        img5.style.display = 'none';
    } else if (xmin == 4) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'block';
        // msg3.style.display = 'none';
        // msg4.style.display = 'none';
        // msg5.style.display = 'none';
        img1.style.display = 'none';
        img2.style.display = 'block';
        img3.style.display = 'none';
        img4.style.display = 'none';
        img5.style.display = 'none';

    } else if (xmin == 3) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'none';
        // msg3.style.display = 'block';
        // msg4.style.display = 'none';
        // msg5.style.display = 'none';
        img1.style.display = 'none';
        img2.style.display = 'none';
        img3.style.display = 'block';
        img4.style.display = 'none';
        img5.style.display = 'none';

    } else if (xmin == 2) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'none';
        // msg3.style.display = 'none';
        // msg4.style.display = 'block';
        // msg5.style.display = 'none';
        img1.style.display = 'none';
        img2.style.display = 'none';
        img3.style.display = 'none';
        img4.style.display = 'block';
        img5.style.display = 'none';

    } else if (xmin == 1) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'none';
        // msg3.style.display = 'none';
        // msg4.style.display = 'none';
        // msg5.style.display = 'block';
        img1.style.display = 'none';
        img2.style.display = 'none';
        img3.style.display = 'none';
        img4.style.display = 'none';
        img5.style.display = 'block';

    }


}, 1000 * 10);

gaugeox.setTextField(document.getElementById("gauge1-txt"), 2);
gaugeph.setTextField(document.getElementById("gauge2-txt"), 2);
gaugete.setTextField(document.getElementById("gauge3-txt"), 2);
gaugeco.setTextField(document.getElementById("gauge4-txt"), 2);


// // clock
function pad(num) {
    return String("0" + num).slice(-2);
}

function updateClock() {
    var now = new Date();
    var time = pad(now.getHours()) + ':' + pad(now.getMinutes());
    var day = pad(now.getDate()) + "/" + pad(now.getMonth() + 1) + "/" + (now.getFullYear());
    document.getElementById('current').innerHTML = day + " - " + time;
}
window.onload = function() {
    setInterval(updateClock, 1000 * 60); // use a shorter interval for better update
}
