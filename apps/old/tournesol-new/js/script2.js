Highcharts.setOptions({
	global: {
		useUTC: false
	}
});

$.ajaxSetup({
	async: false
});


var chartlineox = Highcharts.chart({
	chart: {
		type: 'line',
		renderTo: 'chart_oxygen',
		backgroundColor: '#ffffff00',
		zoomType: 'x',
		height: '75%',
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},

	xAxis: {
		type: 'datetime',
		grid: { // default setting
			enabled: true
		},
		tickWidth: 1,

		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: '',
	},
	yAxis: {
		endOnTick: false,

		// min: 0,
		// max: 10,

		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: '',
	},
	title: '',
	colorAxis: {
		min: 0,
		max: 10,
		stops: [
			[0.1, '#d62728'],
			[0.39, '#d62728'],
			[0.4, '#ff7f0e'],
			[0.49, '#ff7f0e'],
			[0.5, '#DDDF0D'],
			[0.799, '#DDDF0D'],
			[0.8, '#2ca02c'],
			[0.849, '#2ca02c'],
			[0.850, '#1f77b4'],
			[1, '#1f77b4'],
		],
		tickPositions: [0, 4, 5, 8, 10]
	},
	legend: {
		layout: 'horizontal',
		reversed: false,
		floating: false,
		verticalAlign: 'top',
		align: 'center',
	},
	plotOptions: {
		series: {
			dataLabels: {
				enabled: true,
				formatter: function() {
					lastdate1 = Highcharts.dateFormat('%I:%M %p', this.x);;
					val1 = this.y;
					return null;
				}
			}
		}
	},
	series: [{
		showInLegend: false,
		data: [
			[null, null]
		],
		zones: [{
			value: 0,
			color: '#d62728'
		}, {
			value: 3.9,
			color: '#d62728'
		}, {
			value: 4,
			color: '#ff7f0e'
		}, {
			value: 4.9,
			color: '#ff7f0e'
		}, {
			value: 5,
			color: '#DDDF0D'
		}, {
			value: 7.99,
			color: '#DDDF0D'
		}, {
			value: 8,
			color: '#2ca02c'
		}, {
			value: 8.49,
			color: '#2ca02c'
		}, {
			value: 8.5,
			color: '#1f77b4'
		}]
	}],

});

var chartlineph = Highcharts.chart({
	chart: {
		renderTo: 'chart_ph',
		//   type: 'line',
		backgroundColor: '#ffffff00',
		height: '75%', // (9 / 16 * 100) + '%', // 16:9 ratio

		zoomType: 'x',
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},
	time: {
		useUTC: false
	},
	title: {
		text: ''
	},
	xAxis: {
		type: 'datetime',
		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},

	},
	yAxis: {
		endOnTick: false,
		// min: 4,
		// max: 11,
		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: '',

	},
	colorAxis: {
		min: 4.5,
		max: 10.5,
		stops: [
			[0, '#ff7f0e'],
			[(5.49 - 4.5) / 6, '#ff7f0e'],
			[(5.5 - 4.5) / 6, '#DDDF0D'],
			[(5.99 - 4.5) / 6, '#DDDF0D'],
			[(6 - 4.5) / 6, '#2ca02c'],
			[(6.49 - 4.5) / 6, '#2ca02c'],
			[(6.5 - 4.5) / 6, '#1f77b4'],
			[(8.49 - 4.5) / 6, '#1f77b4'],
			[(8.5 - 4.5) / 6, '#2ca02c'],
			[(8.99 - 4.5) / 6, '#2ca02c'],
			[(9 - 4.5) / 6, '#DDDF0D'],
			[(9.49 - 4.5) / 6, '#DDDF0D'],
			[(9.5 - 4.5) / 6, '#ff7f0e'],
			[(1 - 4.5) / 6, '#ff7f0e'],
		],
		tickPositions: [4.5, 6, 9, 10.5]
	},
	legend: {
		layout: 'horizontal',
		reversed: false,
		floating: false,
		verticalAlign: 'top',
		align: 'center',

	},
	plotOptions: {
		series: {
			dataLabels: {
				enabled: true,
				formatter: function() {
					lastdate2 = Highcharts.dateFormat('%I:%M %p', this.x);;
					val2 = this.y;
					return null;
				}
			}
		}
	},
	series: [{
		showInLegend: false,
		data: [
			[null, null]
		],
		zones: [{
			value: 4.5,
			color: '#ff7f0e'
		}, {
			value: 5.5,
			color: '#ff7f0e'
		}, {
			value: 5.51,
			color: '#DDDF0D'
		}, {
			value: 6,
			color: '#DDDF0D'
		}, {
			value: 6.01,
			color: '#2ca02c'
		}, {
			value: 6.49,
			color: '#2ca02c'
		}, {
			value: 6.5,
			color: '#1f77b4'
		}, {
			value: 8.49,
			color: '#1f77b4'
		}, {
			value: 8.5,
			color: '#2ca02c'
		}, {
			value: 8.99,
			color: '#2ca02c'
		}, {
			value: 9,
			color: '#DDDF0D'
		}, {
			value: 9.49,
			color: '#DDDF0D'
		}, {
			value: 9.5,
			color: '#ff7f0e'
		}, {
			value: 11,
			color: '#ff7f0e'
		}, ]
	}],

});


var chartlinetemp = Highcharts.chart({
	chart: {
		renderTo: 'chart_temperature',
		type: 'line',
		backgroundColor: '#ffffff00',
		height: '75%', // (9 / 16 * 100) + '%', // 16:9 ratio
		zoomType: 'x',
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},
	time: {
		useUTC: false
	},
	title: {
		text: ''
	},
	xAxis: {
		type: 'datetime',
		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: {
			text: ''
		},
	},
	yAxis: {
		// min: 15, 
		// max: 35,
		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: {
			text: ''
		}
	},
	rangeSelector: {
		buttons: [{
			text: '+',
			events: {
				click() {
					return false
				}
			}
		}, {
			text: '-',
			events: {
				click() {
					return false
				}
			}

		}]
	},
	colorAxis: {
		min: 0,
		max: 35,
		stops: [
			[0, '#1f77b4'],
			[(19.9) / 35, '#1f77b4'],
			[20 / 35, '#2ca02c'],
			[(22.9) / 35, '#2ca02c'],
			[(23) / 35, '#DDDF0D'],
			[(24.9) / 35, '#DDDF0D'],
			[25 / 35, '#ff7f0e'],
			[(27.9) / 35, '#ff7f0e'],
			[28 / 35, '#d62728'],
			[1, '#d62728'],

		],
		tickPositions: [0, 20, 28, 35]
	},
	legend: {
		layout: 'horizontal',
		reversed: false,
		floating: false,
		verticalAlign: 'top',
		align: 'center',

	},
	plotOptions: {
		series: {
			dataLabels: {
				enabled: true,
				formatter: function() {
					lastdate3 = Highcharts.dateFormat('%I:%M %p', this.x);
					val3 = this.y;
					return null;
				}
			}
		}
	},
	series: [{
		showInLegend: false,
		data: [
			[null, null]
		],
		zones: [{
			value: 0,
			color: '#1f77b4'
		}, {
			value: 19.9,
			color: '#1f77b4'
		}, {
			value: 20,
			color: '#2ca02c'
		}, {
			value: 22.9,
			color: '#2ca02c'
		}, {
			value: 23,
			color: '#DDDF0D'
		}, {
			value: 24.9,
			color: '#DDDF0D'
		}, {
			value: 25,
			color: '#ff7f0e'
		}, {
			value: 27.9,
			color: '#ff7f0e'
		}, {
			value: 28,
			color: '#d62728'
		}, {
			value: 35,
			color: '#d62728'
		}, ]
	}]
});

var chartlineturb = Highcharts.chart({

	chart: {
		renderTo: 'chart_conductivity',
		type: 'line',
		backgroundColor: '#ffffff00',
		height: '75%',
		zoomType: 'x',
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},
	time: {
		useUTC: false
	},
	title: {
		text: ''
	},
	xAxis: {
		type: 'datetime',
		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: {
			text: ''
		},
	},
	yAxis: {
		// min: 15, 
		// max: 35,
		labels: {
			useHTML: true,
			style: {
				fontSize: '13px'
			}
		},
		title: {
			text: ''
		}
	},
	rangeSelector: {
		buttons: [{
			text: '+',
			events: {
				click() {
					return false
				}
			}
		}, {
			text: '-',
			events: {
				click() {
					return false
				}
			}

		}]
	},
	colorAxis: {
		min: 0,
		max: 1600,
		stops: [
			[0, '#1f77b4'],
			[(169) / 1600, '#1f77b4'],
			[170 / 1600, '#2ca02c'],
			[699 / 1600, '#2ca02c'],
			[(700) / 1600, '#DDDF0D'],
			[(1119) / 1600, '#DDDF0D'],
			[1120 / 1600, '#ff7f0e'],
			[(1349) / 1600, '#ff7f0e'],
			[1350 / 1600, '#d62728'],
			[1, '#d62728'],

		],
		tickPositions: [0, 700, 1120, 1600]
	},
	legend: {
		layout: 'horizontal',
		reversed: false,
		floating: false,
		verticalAlign: 'top',
		align: 'center',

	},
	plotOptions: {
		series: {
			dataLabels: {
				enabled: true,
				formatter: function() {
					lastdate4 = Highcharts.dateFormat('%I:%M %p', this.x);
					val4 = this.y;
					return null;
				}
			}
		}
	},
	series: [{
		showInLegend: false,
		data: [
			[null, null],
		],
		zones: [{
			value: 100,
			color: '#1f77b4'
		}, {
			value: 169,
			color: '#1f77b4'
		}, {
			value: 170,
			color: '#2ca02c'
		}, {
			value: 699,
			color: '#2ca02c'
		}, {
			value: 700,
			color: '#DDDF0D'
		}, {
			value: 1119,
			color: '#DDDF0D'
		}, {
			value: 1120,
			color: '#ff7f0e'
		}, {
			value: 1349,
			color: '#ff7f0e'
		}, {
			value: 1350,
			color: '#d62728'
		}, {
			value: 1600,
			color: '#d62728'
		}, ]
	}]
});


var i1, i2, i3, i4;
i1 = 0;
i2 = 0;
i3 = 0
i4 = 0;
function getId(element) {
	var parentClass = element.closest(".sidebar-link"); // Get the parent element with class "myClass"
	var elementId = element.id; // Get the ID of the clicked element
	console.log(elementId); // Print the ID to the console (optional)
	// Pass the ID as a variable to another function or do something else with it
	
	get_urls(elementId);
  }

function get_urls(loc1){
	var url1 = 'https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_'
	var url2 = '%22%20WHERE%20(%22location%22%20%3D%20%27'
	var url3 = '%27)%20AND%20time%20%3E%3D%20now()%20-%2010d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(10m)%2C%20%22device_name%22%20fill(none)&epoch=ms'

	var data1 = 'temperature'
	var data2 = 'dissolved_oxygen'
	var data3 = 'conductivity'
	var data4 = 'pH'
	
	loc = loc1 
	console.log(loc1)
	if (loc === undefined){
		loc = tournesol
	} ;
	if (loc == 'tournesol'){
		color1 = '#DF6D26';
		color2 = '#FBD204';
		title = 'Météo des Étangs de Boitsfort'
	} else if (loc =='bryc'){
		color1 = '#655097';
		color2 = '#4AAFE4';
		title = 'Météo du Port de Bruxelles'
	}else if (loc =='bryc2'){
		color1 = '#0049B7';
		color2 = '#e5eaf5';
		title = 'Météo du Port de Bruxelles 2'
	} else if (loc =='wiels'){
		color1 = '#416129'
		color2='#DF6D26';
		title = 'Météo du Marais Wiels'
	} else if (loc =='leybeek'){
		color1 = '#2E383F';
		color2 = '#A6D6CD';
		title = 'Météo des Étangs du Leybeek'
	} else if (loc =='silex'){
		color1 = '#a28089';
		color2 = '#edf7f6';
		title = 'Météo du Silex'
	} else if (loc =='josaphat'){
		color1 = '#a0d2eb';
		color2 = '#8458B3';
		title = 'Météo des Étangs du parc Josaphat'
	}
	document.documentElement.style.setProperty('--bg', color1);
	document.documentElement.style.setProperty('--accent', color2);
	document.getElementById("poi").innerHTML = title;
	url_temp = url1.concat(data1, url2, loc, url3)
	url_ox = url1.concat(data2, url2, loc, url3)
	url_cond = url1.concat(data3, url2, loc, url3)
	url_ph = url1.concat(data4, url2, loc, url3)
	everything(url_cond, url_ox, url_ph, url_temp)
}

function everything(url_cond, url_ox, url_ph, url_temp) {
	// var xmin, q1, q2, q3, q4, img1, img2, img3, img4, img5;
	// var y1, y2, y3, y4, x;
	img1 = document.getElementById("myimg1");
	img2 = document.getElementById("myimg2");
	img3 = document.getElementById("myimg3");
	img4 = document.getElementById("myimg4");
	img5 = document.getElementById("myimg5");

	

	$.getJSON(url_ox,
		function(dataox) {
			i1 = i1 + 1;
			if (i1 > -1) {
				chartlineox.series[0].setData(dataox.results[0].series[0].values);
			};
			x = dataox.results[0].series[0].values[dataox.results[0].series[0].values.length - 1][0];
			y1 = dataox.results[0].series[0].values[dataox.results[0].series[0].values.length - 1][1];
			chartlineox.series[0].setData(dataox.results[0].series[0].values);

			if (chartlineox) {
				if (chartlineox.series[0].length < 10) {
					chartlineox.series[0].addPoint([x, y1], true, false);
				} else {
					chartlineox.series[0].addPoint([x, y1], true, true);
				}
			}
			if (y1 >= 9) {
				q1 = 5;
			} else if (y1 < 9 && y1 >= 8) {
				q1 = 4;
			} else if (y1 < 8 && y1 >= 5) {
				q1 = 3;
			} else if (y1 < 5 && y1 >= 4) {
				q1 = 2;
			} else {
				q1 = 1;
			}
			console.log(q1)
			$('.now').html(Highcharts.dateFormat('%d/%m/%Y - %I:%M %p', x));
			$('.oxygen').html(val1);


		});
	$.getJSON(url_ph,
		function(dataph) {
			i2 = i2 + 1;
			if (i2 > -1) {
				chartlineph.series[0].setData(dataph.results[0].series[0].values);
			};
			x = dataph.results[0].series[0].values[dataph.results[0].series[0].values.length - 1][0];
			y2 = dataph.results[0].series[0].values[dataph.results[0].series[0].values.length - 1][1];
			chartlineph.series[0].setData(dataph.results[0].series[0].values);

			if (chartlineph) {
				if (chartlineph.series[0].length < 10) {
					chartlineph.series[0].addPoint([x, y2], true, false);
				} else {
					chartlineph.series[0].addPoint([x, y2], true, true);
				}
			}
			if (y2 >= 6.5 && y2 <= 8.5) {
				q2 = 5;
			} else if (y2 < 6.5 && y2 >= 6 || y2 > 8.5 && y2 <= 9) {
				q2 = 4;
			} else if (y2 < 6 && y2 >= 5.5 || y2 > 9 && y2 <= 9.5) {
				q2 = 3;
			} else {
				q2 = 2;
			}
			$('#ph').html(val2);
		});
	$.getJSON(url_temp,
		function(datat) {
			i3 = i3 + 1;
			if (i3 > -1) {
				chartlinetemp.series[0].setData(datat.results[0].series[0].values);
			};
			x = datat.results[0].series[0].values[datat.results[0].series[0].values.length - 1][0];

			y3 = datat.results[0].series[0].values[datat.results[0].series[0].values.length - 1][1];
			chartlinetemp.series[0].setData(datat.results[0].series[0].values);
			if (chartlinetemp) {
				if (chartlinetemp.series[0].length < 10) {
					chartlinetemp.series[0].addPoint([x, y3], true, false);
				} else {
					chartlinetemp.series[0].addPoint([x, y3], true, true);
				}
			};
			if (y3 <= 23) {
				q3 = 5;
			} else if (y3 > 23 && y3 <= 25) {
				q3 = 4;
			} else if (y3 > 25 && y3 <= 27.5) {
				q3 = 3;
			} else if (y3 > 27.5 && y3 <= 30) {
				q3 = 2;
			} else {
				q3 = 1;
			}
			$('.temperature').html(val3);
		});
	$.getJSON(url_cond,
		function(datac) {
			i4 = i4 + 1;
			if (i4 > -1) {
				chartlineturb.series[0].setData(datac.results[0].series[0].values);
			};
			x4 = datac.results[0].series[0].values[datac.results[0].series[0].values.length - 1][0];
			y4 = datac.results[0].series[0].values[datac.results[0].series[0].values.length - 1][1];
			chartlineturb.series[0].setData(datac.results[0].series[0].values);
			if (chartlineturb) {
				if (chartlineturb.series[0].length < 10) {
					chartlineturb.series[0].addPoint([x4, y4], true, false);
				} else {
					chartlineturb.series[0].addPoint([x4, y4], true, true);
				}
			};
			if (y4 <= 675) {
				q4 = 5;
			} else if (y4 > 675 && y4 <= 900) {
				q4 = 4;
			} else if (y4 > 900 && y4 <= 1125) {
				q4 = 3;
			} else if (y4 > 1125 && y4 <= 1350) {
				q4 = 2;
			} else {
				q4 = 1;
			}
			$('.conductivity').html(val4);
		});

	// xmin = Math.min(q1, q2, q3, q4);
	xmin = Math.min(q2, q3, q4);

	console.log(y1, q1, y2, q2, y3, q3, y4, q4, xmin);
	if (xmin == 5) {
		$('.qual1').html('très bonne');
		document.getElementById('qual').style.borderLeftColor = '#1f77b4';
		img1.style.display = 'block';
		img2.style.display = 'none';
		img3.style.display = 'none';
		img4.style.display = 'none';
		img5.style.display = 'none';
	} else if (xmin == 4) {
		$('.qual1').html('bonne');
		document.getElementById('qual').style.borderLeftColor = '#2ca02c';
		img1.style.display = 'none';
		img2.style.display = 'block';
		img3.style.display = 'none';
		img4.style.display = 'none';
		img5.style.display = 'none';

	} else if (xmin == 3) {
		$('.qual1').html('moyenne');
		document.getElementById('qual').style.borderLeftColor = '#DDDF0D';
		img1.style.display = 'none';
		img2.style.display = 'none';
		img3.style.display = 'block';
		img4.style.display = 'none';
		img5.style.display = 'none';

	} else if (xmin == 2) {
		$('.qual1').html('médiocre');
		document.getElementById('qual').style.borderLeftColor = '#ff7f0e';
		img1.style.display = 'none';
		img2.style.display = 'none';
		img3.style.display = 'none';
		img4.style.display = 'block';
		img5.style.display = 'none';

	} else if (xmin == 1) {
		$('.qual1').html('mauvaise');
		document.getElementById('qual').style.borderLeftColor = '#d62728';
		img1.style.display = 'none';
		img2.style.display = 'none';
		img3.style.display = 'none';
		img4.style.display = 'none';
		img5.style.display = 'block';

	}
};

setInterval(everything, 1000 * 300);


function pad(num) {
	return String("0" + num).slice(-2);
}

// function updateClock() {
// 	var now = new Date();
// 	var time = pad(now.getHours()) + ':' + pad(now.getMinutes());
// 	var day = pad(now.getDate()) + "/" + pad(now.getMonth() + 1) + "/" + (now.getFullYear());
// 	document.getElementById('current').innerHTML = day + " - " + time;
// }
window.onload = function() {
	everything();
	console.log(y1, y2, y3, y4)
	// setInterval(updateClock, 1000 * 60); // use a shorter interval for better update
}