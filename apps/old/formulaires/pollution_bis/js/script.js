$(function() {
    multi_step_form({
        container: '.sections',
        section: '.section'
    });
});

function getLocationConstant() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onGeoSuccess, onGeoError);
    } else {
        alert("Your browser or device doesn't support Geolocation");
    }
}

// If we have a successful location update
function onGeoSuccess(event) {
    document.getElementById("Latitude").value = event.coords.latitude;
    document.getElementById("Longitude").value = event.coords.longitude;

}

// If something has gone wrong with the geolocation request
function onGeoError(event) {
    alert("Error code " + event.code + ". " + event.message);
}

$(document).ready(function() {

    $(".anon").on("click", function() {
        var $this = $(this);

        if ($this.is(':checked')) {
            $(".name-field").val("Remain anonymus");
            $(".email-field").val("");
            $(".password-field").val("");
            $(".remain-anonymous").hide();

        } else {
            $(".name-field").val("Name");
            $(".email-field").val("Email");
            $(".password-field").val("Password");
            $(".remain-anonymous").show()
        }

    });

});

$(document).ready(function() {

    $(".image").on("click", function() {
        var $this = $(this);

        if ($this.is(':checked')) {
            $(".photo").val("Non");
            $(".dont-upload").hide();

        } else {
            $(".photo").val("img");
            $(".dont-upload").show()
        }

    });

});