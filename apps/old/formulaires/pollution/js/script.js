function getLocationConstant() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onGeoSuccess, onGeoError);
    } else {
        alert("Your browser or device doesn't support Geolocation");
    }
}

// If we have a successful location update
function onGeoSuccess(event) {
    document.getElementById("Latitude").value = event.coords.latitude;
    document.getElementById("Longitude").value = event.coords.longitude;
}

// If something has gone wrong with the geolocation request
function onGeoError(event) {
    alert("Error code " + event.code + ". " + event.message);
}

$(function() {
    var email = "anon@anonymous.com",
        name = "anonymous";
    $('.anon').change(function() {
        if ($(this).is(":checked")) {
            $('.remain-anonymous').hide();
            $('.name-field').val(name);
            $('.email-field').val(email);
        } else {
            $('.remain-anonymous').show();
            $('.name-field').val("");
            $('.email-field').val("");
        }
    });
});


$(document).ready(function() {

    $(".image").on("click", function() {
        var $this = $(this);

        if ($this.is(':checked')) {
            $(".photo").val("Non");
            $(".dont-upload").hide();

        } else {
            $(".photo").val("img");
            $(".dont-upload").show()
        }

    });

});

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 2)) {
        document.getElementById("nextBtn").innerHTML = "Summary";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
        // ... the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false
            valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
}


const form = document.getElementById('regForm');
const summary = document.getElementById('step-summary');
const clear = document.getElementById('step-clear');

// Remove all children of the summary list.
function clearSummary() {
    while (summary.firstElementChild) {
        summary.firstElementChild.remove();
    }
}

// Clear list on click.
clear.addEventListener('click', event => {
    clearSummary();
});

form.addEventListener('submit', event => {
    // Clear list first.
    clearSummary();

    // Create a fragment to store the list items in.
    // Get the data from the form.
    const fragment = new DocumentFragment();
    const formData = new FormData(event.target);

    // Turn each entry into a list item which display
    // the name of the input and its value.
    // Add each list item to the fragment.
    for (const [name, value] of formData) {
        const listItem = document.createElement('li');
        listItem.textContent = `${name}: ${value}`;
        fragment.appendChild(listItem);
    }

    // Add all list items to the summary.
    summary.appendChild(fragment);
    event.preventDefault();
});

// var i, $mvar = $('.form-title');

// var $classes = [];

// $('body *:not(script)').each(function() {
//     _classes = $(this).attr('class') ? $(this).attr('class').split(' ') : []
//     _classes.forEach(function(entry) {
//         if ($classes.indexOf(entry) < 0) {
//             $classes.push(entry)
//         }
//     })
// })

// var $uniqueClasses = [...new Set($classes)]

// function logit(string) {
//     var text = document.createTextNode(string);
//     $('#outige').append(text);
//     $('#outige').append("<br>");
// }

// for (i = 0; i < $mvar.length; i++) {
//     logit($mvar.eq(i).html());
// }